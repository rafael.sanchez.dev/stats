package com.rsr.stats.rps.dao;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.rsr.stats.rps.model.RpsRoundResult;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class RpsResultsDaoTest {

    @InjectMocks
    RpsResultsDao dao;

    @Test
    public void storeResultReturnSuccessWhenResultIsDraw() {

        dao.storeResult(RpsRoundResult.DRAW);

        assertThat(dao.getTotalResults(RpsRoundResult.DRAW), is(1));
    }

    @Test
    public void storeResultReturnSuccessWhenResultIsPlayer1Wins() {

        dao.storeResult(RpsRoundResult.PLAYER1WINS);

        assertThat(dao.getTotalResults(RpsRoundResult.PLAYER1WINS), is(1));
    }

    @Test
    public void storeResultReturnSuccessWhenResultIsPlayer2Wins() {

        dao.storeResult(RpsRoundResult.PLAYER2WINS);

        assertThat(dao.getTotalResults(RpsRoundResult.PLAYER2WINS), is(1));
    }

    @Test
    public void getTotalResultsForReturnsZeroWhenEmptyForDraw() {

        assertThat(dao.getTotalResults(RpsRoundResult.DRAW), is(0));
    }

    @Test
    public void getTotalResultsForReturnsZeroWhenEmptyForPlayer1Wins() {

        assertThat(dao.getTotalResults(RpsRoundResult.PLAYER1WINS), is(0));
    }

    @Test
    public void getTotalResultsForReturnsZeroWhenEmptyForPlayer2Wins() {

        assertThat(dao.getTotalResults(RpsRoundResult.PLAYER2WINS), is(0));
    }
}
