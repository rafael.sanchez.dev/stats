package com.rsr.stats.rps.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.rsr.stats.rps.dao.RpsResultsDao;
import com.rsr.stats.rps.model.RpsRoundResult;

/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class RpsStatisticsServiceTest {

    @Mock
    private RpsMessageParser rpsMessageParser;

    @Mock
    private RpsResultsDao rpsResultsDao;

    @InjectMocks
    RpsStatisticsService rpsStatisticsService;


    @Test
    public void addRpsStatisticsReturnSuccessWhenMessageIsIncorrect() {
        when(rpsMessageParser.parseRpsMessage("Invalid message")).thenThrow(IllegalArgumentException.class);

        rpsStatisticsService.addStatistics("Invalid message");

        verifyZeroInteractions(rpsResultsDao);
    }

    @Test
    public void addStatisticsReturnSuccessWhenMessageIsDraw() {
        when(rpsMessageParser.parseRpsMessage(RpsRoundResult.DRAW.getMessage())).thenReturn(RpsRoundResult.DRAW);

        rpsStatisticsService.addStatistics(RpsRoundResult.DRAW.getMessage());

        verify(rpsResultsDao).storeResult(RpsRoundResult.DRAW);
    }

    @Test
    public void addStatisticsReturnSuccessWhenMessageIsPlayer1Wins() {
        when(rpsMessageParser.parseRpsMessage(RpsRoundResult.PLAYER1WINS.getMessage())).thenReturn(RpsRoundResult.PLAYER1WINS);

        rpsStatisticsService.addStatistics(RpsRoundResult.PLAYER1WINS.getMessage());

        verify(rpsResultsDao).storeResult(RpsRoundResult.PLAYER1WINS);
    }

    @Test
    public void addStatisticsReturnSuccessWhenMessageIsPlayer2Wins() {
        when(rpsMessageParser.parseRpsMessage(RpsRoundResult.PLAYER2WINS.getMessage())).thenReturn(RpsRoundResult.PLAYER2WINS);

        rpsStatisticsService.addStatistics(RpsRoundResult.PLAYER2WINS.getMessage());

        verify(rpsResultsDao).storeResult(RpsRoundResult.PLAYER2WINS);
    }

    @Test
    public void getStatisticsReturnsStats() {
        when(rpsResultsDao.getTotalResults(RpsRoundResult.DRAW)).thenReturn(1);
        when(rpsResultsDao.getTotalResults(RpsRoundResult.PLAYER1WINS)).thenReturn(1);
        when(rpsResultsDao.getTotalResults(RpsRoundResult.PLAYER2WINS)).thenReturn(1);

        RpsStatisticsResponse response = rpsStatisticsService.getStatistics();

        assertThat(response.getTotalRoundPlayed(), is(3));
        assertThat(response.getTotalDraws(), is(1));
        assertThat(response.getTotalPlayer1Wins(), is(1));
        assertThat(response.getTotalPlayer2Wins(), is(1));
    }

}
