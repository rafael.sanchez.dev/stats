package com.rsr.stats.rps.service;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.rsr.stats.rps.model.RpsRoundResult;


/**
 * @author Rafa Sanchez
 */
@RunWith(MockitoJUnitRunner.class)
public class RpsMessageParserTest {

    @InjectMocks
    RpsMessageParser parser;

    @Test
    public void valueOfReturnsDRAWWhenMessageIsDraw() {

        RpsRoundResult result = parser.parseRpsMessage("Draw");

        assertThat(result, is(RpsRoundResult.DRAW));
    }

    @Test
    public void valueOfReturnsPLAYER1WINSWhenMessageIsPlayer1Wins() {

        RpsRoundResult result = parser.parseRpsMessage("Player 1 Wins");

        assertThat(result, is(RpsRoundResult.PLAYER1WINS));
    }

    @Test
    public void valueOfReturnsPLAYER2WINSWhenMessageIsPlayer2Wins() {
        RpsRoundResult result = parser.parseRpsMessage("Player 2 Wins");

        assertThat(result, is(RpsRoundResult.PLAYER2WINS));
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueOfThrowsExceptionWhenMessageIsInvalid() {
        parser.parseRpsMessage("Invalid");
    }

}
