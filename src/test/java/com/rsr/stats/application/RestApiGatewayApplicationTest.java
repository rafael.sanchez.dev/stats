package com.rsr.stats.application;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.rsr.stats.rps.service.RpsStatisticsResponse;
import com.rsr.stats.rps.service.RpsStatisticsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RestApiGatewayApplicationTest {
    private static final String RPS_STATISTICS_REQUEST = "/rpsstatistics";
    private static final String JSON_RESPONSE = "{'totalRoundPlayed':4, 'totalDraws':1, 'totalPlayer1Wins':1, 'totalPlayer2Wins':2}";

    @MockBean
	private RpsStatisticsService service;

	@Autowired
	private WebApplicationContext context;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void getStatisticsReturnSuccess() throws Exception {

		when(service.getStatistics()).thenReturn(buildRpsStatisticsResponse());

		this.mockMvc.perform(get(RPS_STATISTICS_REQUEST))
		            .andExpect(status().is2xxSuccessful())
		            .andExpect(content().json(JSON_RESPONSE))
		            .andReturn();
	}

	private RpsStatisticsResponse buildRpsStatisticsResponse() {
		return RpsStatisticsResponse.builder().totalRoundPlayed(4).totalPlayer1Wins(1).totalPlayer2Wins(2).totalDraws(1).build();
	}

}
