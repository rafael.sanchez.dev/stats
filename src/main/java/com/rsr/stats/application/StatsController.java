package com.rsr.stats.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rsr.stats.rps.service.RpsStatisticsResponse;
import com.rsr.stats.rps.service.RpsStatisticsService;

/**
 * @author Rafa Sanchez
 */
@RestController
public class StatsController {

    private final RpsStatisticsService rpsStatisticsService;
    private final ResponseContentBuilder<RpsStatisticsResponse> responseContentBuilder;

    @Autowired
    public StatsController(RpsStatisticsService rpsStatisticsService, ResponseContentBuilder<RpsStatisticsResponse> responseContentBuilder) {
        this.rpsStatisticsService = rpsStatisticsService;
        this.responseContentBuilder = responseContentBuilder;
    }

    @GetMapping(value = "/rpsstatistics")
    public ResponseEntity<RpsStatisticsResponse> getRpsStatistics() {
        return responseContentBuilder.buildResponse(HttpStatus.OK, rpsStatisticsService.getStatistics());
    }
}
