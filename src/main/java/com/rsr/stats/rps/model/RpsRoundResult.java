package com.rsr.stats.rps.model;

import java.util.Arrays;

/**
 * @author Rafa Sanchez
 */
public enum RpsRoundResult {
    DRAW("Draw"),
    PLAYER1WINS("Player 1 wins"),
    PLAYER2WINS("Player 2 wins");

    private String message;

    RpsRoundResult(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public static RpsRoundResult fromString(String message) {
        return Arrays.stream(RpsRoundResult.values())
                     .filter(roundResult -> roundResult.getMessage().equalsIgnoreCase(message))
                     .findFirst()
                     .orElseThrow(() -> new IllegalArgumentException("No constant with text " + message + " found"));
    }
}
