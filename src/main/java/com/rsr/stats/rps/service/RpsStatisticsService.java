package com.rsr.stats.rps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rsr.stats.rps.dao.RpsResultsDao;
import com.rsr.stats.rps.model.RpsRoundResult;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Rafa Sanchez
 */
@Slf4j
@Service
public class RpsStatisticsService {

    private final RpsMessageParser rpsMessageParser;
    private final RpsResultsDao rpsResultsDao;

    @Autowired
    public RpsStatisticsService(RpsMessageParser rpsMessageParser, RpsResultsDao rpsResultsDao) {
        this.rpsMessageParser = rpsMessageParser;
        this.rpsResultsDao = rpsResultsDao;
    }


    public void addStatistics(String message) {
        try {
            RpsRoundResult roundResult = rpsMessageParser.parseRpsMessage(message);
            storeStatistics(roundResult);
        } catch(IllegalArgumentException e) {
            log.error("Cannot add statistics because the message is incorrect - {}", e.getMessage());
        }
    }

    public RpsStatisticsResponse getStatistics() {
        int totalDraws = rpsResultsDao.getTotalResults(RpsRoundResult.DRAW);
        int totalPlayer1Wins = rpsResultsDao.getTotalResults(RpsRoundResult.PLAYER1WINS);
        int totalPlayer2Wins = rpsResultsDao.getTotalResults(RpsRoundResult.PLAYER2WINS);
        return buildStatisticsResponse(totalDraws, totalPlayer1Wins, totalPlayer2Wins);
    }

    private void storeStatistics(RpsRoundResult roundResult) {
        rpsResultsDao.storeResult(roundResult);
    }

    private RpsStatisticsResponse buildStatisticsResponse(int totalDraws, int totalPlayer1Wins, int totalPlayer2Wins) {
        return RpsStatisticsResponse.builder().totalDraws(totalDraws)
                                    .totalPlayer1Wins(totalPlayer1Wins)
                                    .totalPlayer2Wins(totalPlayer2Wins)
                                    .totalRoundPlayed(totalDraws + totalPlayer1Wins + totalPlayer2Wins)
                                    .build();
    }
}
