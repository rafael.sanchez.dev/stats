package com.rsr.stats.rps.service;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.brainlag.nsq.NSQMessage;
import com.rsr.stats.rps.model.RpsRoundResult;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RpsMessageParser {
    private final ObjectMapper mapper = new ObjectMapper();

    public RpsRoundResult parseRpsMessage(String message) {
        return RpsRoundResult.fromString(message);
    }

    public String parseNSQRpsMessage(NSQMessage message) {
        try {
            return mapper.readValue(message.getMessage(), String.class);
        } catch (IOException ex) {
            log.error("Exception {} during parsing input message {} ", ex.getMessage(), message);
        }
        return null;
    }
}