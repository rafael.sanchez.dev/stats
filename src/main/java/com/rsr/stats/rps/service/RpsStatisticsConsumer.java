package com.rsr.stats.rps.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.NSQMessage;
import com.rsr.stats.common.nsq.NSQAdminService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Rafa Sanchez
 */
@Component
@Slf4j
public class RpsStatisticsConsumer {

    private final RpsStatisticsService rpsStatisticsService;
    private final NSQAdminService nsqAdminService;
    private final RpsMessageParser parser;

    @Autowired
    public RpsStatisticsConsumer(RpsStatisticsService rpsStatisticsService, NSQAdminService nsqAdminService, RpsMessageParser parser) {
        this.rpsStatisticsService = rpsStatisticsService;
        this.nsqAdminService = nsqAdminService;
        this.parser = parser;
    }
    @Bean
    public NSQConsumer registerRpsStatisticsConsumer() {
        return nsqAdminService.registerConsumer(this::processMessage);
    }

    private void processMessage(NSQMessage message) {
        String rpsRoundResult = parser.parseNSQRpsMessage(message);
        log.info("Message received  : {}", rpsRoundResult);
        if (!StringUtils.isEmpty(rpsRoundResult)) {
            rpsStatisticsService.addStatistics(rpsRoundResult);
        } else {
            log.warn("Message discarded because is empty");
        }
        message.finished();
    }
}
