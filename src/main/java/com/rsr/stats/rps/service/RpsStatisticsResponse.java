package com.rsr.stats.rps.service;

import lombok.Builder;
import lombok.Getter;

/**
 * @author Rafa Sanchez
 */
@Builder
@Getter
public class RpsStatisticsResponse {
    private int totalRoundPlayed;
    private int totalDraws;
    private int totalPlayer1Wins;
    private int totalPlayer2Wins;
}
