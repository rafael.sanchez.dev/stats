package com.rsr.stats.rps.dao;

import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.stereotype.Component;

import com.rsr.stats.rps.model.RpsRoundResult;

/**
 * @author Rafa Sanchez
 */
@Component
public class RpsResultsDao {

    private final ConcurrentMap<RpsRoundResult, Integer> roundResultStore = new ConcurrentHashMap<>();

    public RpsResultsDao() {
        Arrays.stream(RpsRoundResult.values()).forEach(roundResult -> roundResultStore.put(roundResult, 0));
    }

    public void storeResult(RpsRoundResult roundResult) {
        roundResultStore.compute(roundResult, (key, value) -> value + 1);
    }

    public int getTotalResults(RpsRoundResult roundResult) {
        return roundResultStore.get(roundResult);
    }
}
