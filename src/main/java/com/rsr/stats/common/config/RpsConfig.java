package com.rsr.stats.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;

/**
 * @author Rafa Sanchez
 */
@Getter
@Component
public class RpsConfig {

    @Value("${statistics.nsq.topic}")
    private String statisticsTopic;

    @Value("${statistics.nsq.lookup.host}")
    private String statisticsLookupHost;

    @Value("${statistics.nsq.lookup.port}")
    private int statisticsLookupPort;

    @Value("${statistics.nsq.consumer.channel}")
    private String statisticsConsumerChannel;
}
