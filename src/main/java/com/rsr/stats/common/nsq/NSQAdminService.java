package com.rsr.stats.common.nsq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.brainlag.nsq.NSQConsumer;
import com.github.brainlag.nsq.callbacks.NSQMessageCallback;
import com.github.brainlag.nsq.lookup.DefaultNSQLookup;
import com.github.brainlag.nsq.lookup.NSQLookup;
import com.rsr.stats.common.config.RpsConfig;

import lombok.extern.slf4j.Slf4j;

/**
 * @author Rafa Sanchez
 */

@Service
@Slf4j
public class NSQAdminService {

    private RpsConfig config;

    @Autowired
    public NSQAdminService(RpsConfig config) {
        this.config = config;
    }

    public NSQConsumer registerConsumer(NSQMessageCallback nsqMessageCallback) {
        log.info("Registering NSQ Statistics consumer for ......{}", config.getStatisticsTopic());
        NSQConsumer consumer = new NSQConsumer(
                setNSQLookup(), config.getStatisticsTopic(), config.getStatisticsConsumerChannel(), nsqMessageCallback);
        consumer.start();
        return consumer;
    }

    private NSQLookup setNSQLookup() {
        NSQLookup lookup = new DefaultNSQLookup();
        lookup.addLookupAddress(config.getStatisticsLookupHost(), config.getStatisticsLookupPort());
        return lookup;
    }

}
